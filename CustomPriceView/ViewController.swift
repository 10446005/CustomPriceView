//
//  ViewController.swift
//  CustomPriceView
//
//  Created by 謝佳瑋 on 2018/8/25.
//  Copyright © 2018年 jason. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var showButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        showButton.addTarget(self, action: #selector(ViewController.tapAction), for: .touchUpInside)
        // Do any additional setup after loading the view, typically from a nib.
        hidesBottomBarWhenPushed = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    @objc func tapAction() {
        let views = SeasonProductDetailAddBasketView.makeViews()
        views.basketView.setData()
        views.basketView.show(with: self)
    }

}

