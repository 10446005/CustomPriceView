//
//  MasterViewController.swift
//  CustomPriceView
//
//  Created by 謝佳瑋 on 2018/8/25.
//  Copyright © 2018年 jason. All rights reserved.
//

import UIKit

class MasterViewController: UIViewController {

    @IBOutlet weak var pushButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        hidesBottomBarWhenPushed = true
    }

}
