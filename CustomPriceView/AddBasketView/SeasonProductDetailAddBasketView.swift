//
//  SeasonProductDetailAddBasketView.swift
//  GreenBox
//
//  Created by 謝佳瑋 on 2018/8/23.
//  Copyright © 2018年 soohoobook. All rights reserved.
//

import Material
import UIKit
import Kingfisher
import FontAwesome_swift

class SeasonProductDetailAddBasketView: UIView {
  
  @IBOutlet weak var productImageView: UIImageView!
  @IBOutlet weak var closeImageView: UIImageView!
  
  @IBOutlet weak var minusButton: UIButton!
  @IBOutlet weak var plusButton: UIButton!
  
  
  @IBOutlet weak var priceLabel: UILabel!
  @IBOutlet weak var discountTextLabel: UILabel!
  @IBOutlet weak var remainingQuantityLabel: UILabel!
  @IBOutlet weak var numberBuyersLabel: UILabel!
  @IBOutlet weak var selectNumberTextLabel: UILabel!
  @IBOutlet weak var selectedNumberLabel: UILabel!
  @IBOutlet weak var addBasketButton: RaisedButton!
  @IBOutlet var contentView: UIView!
  
  
  var buyQuantity = 1 {
    didSet {
      selectedNumberLabel.text = "\(buyQuantity)"
    }
  }
  
  private var views: (bgView: UIView, basketView: SeasonProductDetailAddBasketView)?
  
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    //        Bundle.main.loadNibNamed("SeasonProductDetailAddBasketView", owner: self, options: nil)
    setupNib()
    initView()
  }
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setupNib()
    initView()
  }
  
  private func setupNib() {
    let nib = UINib(nibName: "SeasonProductDetailAddBasketView", bundle: nil)
    nib.instantiate(withOwner: self, options: nil)
    contentView.frame = bounds
    addSubview(contentView)
  }
  
  private func initView() {
    discountTextLabel.text = ""
    closeImageView.tintColor = UIColor.mainGreen
    closeImageView.image = #imageLiteral(resourceName: "xxxx-bt").resize(targetSize: CGSize(width: 23, height: 23)).tint(with: UIColor.mainGreen)
    productImageView.image = UIImage.unknownImage.resize(targetSize: CGSize(width: 23, height: 23))
    let plusImg = UIImage.fontAwesomeIcon(name: .plus, style: .solid, textColor: UIColor.mainGreen, size: CGSize(width: 16, height: 16))
    let minusImg = UIImage.fontAwesomeIcon(name: .minus, style: .solid, textColor: UIColor.mainGreen, size: CGSize(width: 16, height: 16))
    plusButton.setImage(plusImg, for: .normal)
    minusButton.setImage(minusImg, for: .normal)
    selectedNumberLabel.text = "1"
    addBasketButton.layer.masksToBounds = true
    addBasketButton.layer.cornerRadius = 5
    closeImageView.isUserInteractionEnabled = true
    let tapAction = UITapGestureRecognizer(target: self, action: #selector(self.close))
    closeImageView.addGestureRecognizer(tapAction)
    plusButton.addTarget(self, action: #selector(self.addTapAction), for: .touchUpInside)
    minusButton.addTarget(self, action: #selector(self.minusTapAction), for: .touchUpInside)
    
    
    let gesture = UITapGestureRecognizer.init(target: self, action: #selector(self.test(_:)))
    gesture.numberOfTapsRequired = 1
    selectedNumberLabel.addGestureRecognizer(gesture)
    
  }
  
  @objc func test(_ sender : UITapGestureRecognizer) {
    print("123")
  }
  
  
  func setData() {
    productImageView.kf.setImage(with: URL(string: "https://fruitweb.azurewebsites.net//upload/Product_3033/201808080458460.jpg"), placeholder: UIImage.unknownImage.resize(targetSize: CGSize(width: 23, height: 23)))
    priceLabel.text = "$450"
    discountTextLabel.text = ""
    remainingQuantityLabel.text = "剩下99盒"
    numberBuyersLabel.text = "50人已購買"
  }
  
  static func makeViews() -> (bgView: UIView, basketView: SeasonProductDetailAddBasketView) {
    let bgview = UIView(frame: UIScreen.main.bounds)
    let basketView = SeasonProductDetailAddBasketView(frame: UIScreen.main.bounds)
    basketView.views = (bgview, basketView)
    return basketView.views!
  }
  
  
  func show(with vc: ViewController) {
    assert(views.isNotNil, "Must be call makeViews().")
    vc.view.layout(views!.bgView).left(0).right(0).center().top(0).bottom(0)
    let vh = UIScreen.main.bounds.height*10/33
//    if UIScreen.main.bounds.height > 600 {

      views?.basketView.frame = CGRect(x: (views?.basketView.frame.origin.x)!, y: (views?.basketView.frame.maxY)!, width: (views?.basketView.frame.width)!, height: vh)
//    }else {
//      views?.basketView.frame = CGRect(x: (views?.basketView.frame.origin.x)!, y: (views?.basketView.frame.maxY)!, width: (views?.basketView.frame.width)!, height: vh)
//    }
    
    
    vc.view.addSubview((views?.basketView)!)
    
    show()
    
  }
  
  @objc func addTapAction() {
    if buyQuantity < 12 {
      buyQuantity += 1
    }
    
  }
  
  @objc func minusTapAction() {
    if buyQuantity > 1 {
      buyQuantity -= 1
    }
  }
  
  func show() {
    assert(views.isNotNil, "Must be call makeViews().")
    self.views!.bgView.frame = UIScreen.main.bounds
    self.views!.bgView.alpha = 0
    self.views!.bgView.backgroundColor = UIColor(netHex: 0x1A1A1A)
    UIView.animate(withDuration: 0.4, animations: {
      self.views!.bgView.alpha = 0.4
    })
    let vh = UIScreen.main.bounds.height*10/33
    //        views!.basketView.frame = CGRect(x: 0, y: UIScreen.main.bounds.height, width: UIScreen.main.bounds.width, height: vh)
    let offset: CGFloat = UIScreen.main.bounds.height > 600 ? 60 : 0
    let ps = CGPoint(x: UIScreen.main.bounds.width/2, y: UIScreen.main.bounds.height-vh+offset)
    
    views!.basketView.animate(.position(ps), .duration(0.4))
    
//    UIView.animate(withDuration: 0.4, animations: {
//      self.views!.basketView.frame = CGRect(x: self.views!.basketView.frame.origin.x,
//                                            y: self.views!.basketView.frame.origin.y - self.views!.basketView.frame.height,
//                                            width: self.views!.basketView.frame.width,
//                                            height: self.views!.basketView.frame.height)
//    })
    
    updateFocusIfNeeded()
    layoutIfNeeded()
  }
  
  @objc
  func close() {
    assert(views.isNotNil, "Must be call makeViews().")
    views!.bgView.animate([.fade(0.0), .duration(0.4)], completion: {
      self.views!.bgView.removeFromSuperview()
    })
    let ps = CGPoint(x: UIScreen.main.bounds.width/2, y: UIScreen.main.bounds.height*1.1)
    views!.basketView.animate([.position(ps), .duration(0.4)], completion: {
      self.views!.basketView.removeFromSuperview()
    })
    
  }
  
}

extension Optional {
  var isNil: Bool {
    return self == nil
  }
  
  var isNotNil: Bool {
    return !self.isNil
  }
}

extension UIColor {
  convenience init(red: Int, green: Int, blue: Int, alpha: CGFloat = 1.0) {
    assert(red >= 0 && red <= 255, "Invalid red component")
    assert(green >= 0 && green <= 255, "Invalid green component")
    assert(blue >= 0 && blue <= 255, "Invalid blue component")
    
    self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
  }
  
  convenience init(netHex: Int, alpha: CGFloat = 1.0) {
    self.init(red:(netHex >> 16) & 0xff, green:(netHex >> 8) & 0xff, blue:netHex & 0xff, alpha: alpha)
  }
  
  static let mainGreen = UIColor(netHex: 0x4ba83b)
  static let unselectedGray = UIColor(netHex: 0x999999)
  static let backgroundGray = UIColor(netHex: 0xf2f2f2)
}

extension UIImage {
  static let unknownImage = UIImage.fontAwesomeIcon(
    name: FontAwesome.image,
    style: .solid,
    textColor: UIColor(netHex: 0x999999, alpha: 0.5),
    size: CGSize(width: 300, height: 200))
}

extension UIImage {
  func resize(targetSize: CGSize) -> UIImage {
    let size = self.size
    
    let widthRatio  = targetSize.width  / size.width
    let heightRatio = targetSize.height / size.height
    
    // Figure out what our orientation is, and use that to form the rectangle
    var newSize: CGSize
    if(widthRatio > heightRatio) {
      newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
    } else {
      newSize = CGSize(width: size.width * widthRatio,  height: size.height * widthRatio)
    }
    
    // This is the rect that we've calculated out and this is what is actually used below
    let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
    
    // Actually do the resizing to the rect using the ImageContext stuff
    UIGraphicsBeginImageContextWithOptions(newSize, false, 0.0)
    self.draw(in: rect)
    let newImage = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()
    
    return newImage!
  }
}

